<?php

namespace App\Http\Controllers\Api;

use App\Commission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use App\Laravue\Models\Permission;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

class CommissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->hasRole('Supreme Agent')) {

            $data = DB::table('commissions as c')
                                ->join('events as e', 'e.id', '=', 'c.event_id')
                                ->join('users as u', 'u.id', '=', 'c.player_id')
                                ->where('supreme_agent_id', $user->id)
                                ->select('c.*', 'e.name as event_name', 'u.name as player_name')
                                ->get();
                                return $data;

        } elseif ($user->hasRole('Master Agent')) {
            $data = DB::table('commissions as c')
                                ->join('events as e', 'e.id', '=', 'c.event_id')
                                ->join('users as u', 'u.id', '=', 'c.player_id')
                                ->where('master_agent_id', $user->id)
                                ->select('c.*', 'e.name as event_name', 'u.name as player_name')
                                ->get();
                                return $data;
        } elseif ($user->hasRole('Agent')) {
            $data = DB::table('commissions as c')
                                ->join('events as e', 'e.id', '=', 'c.event_id')
                                ->join('users as u', 'u.id', '=', 'c.player_id')
                                ->where('agent_id', $user->id)
                                ->select('c.*', 'e.name as event_name', 'u.name as player_name')
                                ->get();
                                return $data;
        } elseif ($user->hasRole('Company')) {
            $data = DB::table('events as e')
                                ->join('event_details as ed', 'ed.event_id', '=', 'e.id')
                                ->select('e.name as event_name', 'ed.*')
                                ->get();

            return $data;
        }
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function show(Commission $commission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function edit(Commission $commission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commission $commission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Commission  $commission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commission $commission)
    {
        //
    }
}
