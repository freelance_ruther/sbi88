<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Wallet;
use App\PlayerWallet;
use DB;
use App\Events\WalletEvent;


class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->hasRole('Company')) {

            $wallet = DB::table('wallets as w')
                            ->join('users as u', 'u.id', '=', 'w.load_to_user_id')
                            ->join('player_wallets as p', 'p.player_id', '=', 'w.load_to_user_id')
                            ->where('w.load_from_user_id', $user->id)
                            ->where('w.transaction_type', 'cash-in')
                           
                            ->select('u.*', 'w.*', 'p.available_balance')
                            ->get();

        } else {

            $wallet = DB::table('wallets as w')
                                   ->join('users as u', 'u.id', '=', 'w.load_to_user_id')
                                   ->where('w.load_from_user_id', Auth::user()->id)
                                   ->select('u.*', 'w.*')
                                   ->get();
        }
        return $wallet;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user = Auth::user();

        $currentBalance;

        if($user->hasRole('Company')) {
            $currentBalance = DB::table('outlays')
                                                ->select('amount as available_balance')
                                                ->get();
        } else {

            $currentBalance = PlayerWallet::where('player_id', $user->id)
                                                ->select('amount as available_balance')
                                                        ->select('available_balance as available_balance')
                                                        ->get();
        }

        

        if($currentBalance[0]->available_balance == 0) {

            return "Insuficient Balance";

        } else {
            $balance = DB::table('player_wallets')
            ->where('player_id', $request->load_to_user_id)
            ->select('available_balance')
            ->get();

            $running_balance = $balance[0]->available_balance + $request->amount;


            $wallet = Wallet::create([
            'amount' => $request->input('amount'),
            'transaction_type' => $request->input('transaction_type'),
            'percentage' => $request->input('percentage'),
            'load_from_user_id' => Auth::user()->id,
            'load_to_user_id' => $request->input('load_to_user_id'),
            'amount_to_be_credited' => $request->input('amount'),
            'running_balance' => $running_balance,
            ]);

            $totalBalance = DB::table('wallets')
                                    ->where('load_to_user_id', $request->input('load_to_user_id'))
                                    ->where('transaction_type', 'cash-in')
                                    ->sum('amount');

            $updateWallet = PlayerWallet::where('player_id', $request->load_to_user_id)
                                                    ->increment('available_balance', $request->amount);

            $updateAgentWallet = PlayerWallet::where('player_id', Auth::user()->id)
                                                    ->decrement('available_balance', $request->amount);

            event(new WalletEvent());
            return $running_balance;
        }
       
    }                           

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
