<?php
/**
 * File AuthController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */
namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Role;
use App\Laravue\Models\User;
/**
 * Class AuthController
 *
 * @package App\Http\Controllers\Api
 */
class AuthController extends BaseController
{

    
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // if (Auth::attempt($credentials)) {
        //     return "k";
        // } else {
        //     return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
        // }

        $getUser = DB::table('users')
                                ->where('email', $request->input('email'))
                                ->select('id')
                                ->get();

       $checkRole = DB::table('users')
                                    ->where('id', $getUser[0]->id)
                                    ->get();

        $checkStatus = DB::table('users')
                                        ->select('active_status')
                                        ->where('id', $getUser[0]->id)
                                        ->get();
        
        if($checkStatus[0]->active_status == 'online') {

            if($getUser[0]->id == 1) {
                if (!Auth::attempt($credentials)) {
                    return response()->json(new JsonResponse([], 'Invalid Credentials'), Response::HTTP_UNAUTHORIZED);
                } 
    
                $updateStatus = User::where('id', $getUser[0]->id)
                ->update(['active_status' => 'online']);
                $user = $request->user();
                
                return response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
            } else {
                return response()->json(new JsonResponse([], 'Already Logged In to Another Device.'), Response::HTTP_UNAUTHORIZED);
                Auth::guard('web')->logout();
            }
           
          
        } else {
            if(count($checkRole) == 0) {
                if (!Auth::attempt($credentials)) {
                   return response()->json(new JsonResponse([], 'Invalid Credentials'), Response::HTTP_UNAUTHORIZED);
               } 
               
               $updateStatus = User::where('id', $getUser[0]->id)
                                                    ->update(['active_status' => 'online']);
               $user = $request->user();
       
               return response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
               
           } else {
               if($checkRole[0]->status == "pending") {
                   return response()->json(new JsonResponse([], 'Account Pending.'), Response::HTTP_UNAUTHORIZED);
               } elseif ($checkRole[0]->status == "deactivated") {
                   return response()->json(new JsonResponse([], 'Account Deactivated.'), Response::HTTP_UNAUTHORIZED);
               } else {
                   if (!Auth::attempt($credentials)) {
                       return response()->json(new JsonResponse([], 'Invalid Credentials'), Response::HTTP_UNAUTHORIZED);
                   } 
       
                   $updateStatus = User::where('id', $getUser[0]->id)
                   ->update(['active_status' => 'online']);
                   $user = $request->user();
                   
                   return response()->json(new JsonResponse(new UserResource($user)), Response::HTTP_OK);
               }
           }
        }

  
        
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $user = Auth::user()->id;
        Auth::guard('web')->logout();
        $updateStatus = User::where('id', $user)
                                        ->update(['active_status' => 'offline']);
            
                    DB::table('sessions')
                                        ->whereUserId($request->user()->id)
                                        ->delete();

        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

}
