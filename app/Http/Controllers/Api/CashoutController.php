<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Cashout;
use Illuminate\Http\Request;
use DB;
use App\Laravue\JsonResponse;
use Illuminate\Http\Response;
use App\Wallet;
use App\PlayerWallet;
use App\Outlay;
use App\Events\WalletEvent;

class CashoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if($user->hasRole('Player')) {
            $data = Cashout::where('player_id', $user->id)->get();
        } else {
            $data = DB::table('cashouts as c')
                                ->join('users as u', 'u.id', '=', 'c.player_id')
                                ->where('agent_id', $user->id)
                                ->select('c.*', 'u.name as member_name')
                                ->get();
                                
        }
       
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $balance = DB::table('player_wallets')
                                    ->where('player_id', Auth::user()->id)
                                    ->select('available_balance')
                                    ->get();

       $balance[0]->available_balance;

       if($request->amount > $balance[0]->available_balance) {
           
       /**
        *  Check if amount is greater than the current balance
        */
        return response()->json(new JsonResponse([], 'Insufficient Balance'), Response::HTTP_UNAUTHORIZED);

       } else {
           /**
            *  Process the request
            *  Get the requestor's agent id
            */

            $user = Auth::user();
            $agent_id;

            if($user->hasRole('Player') ) {

                $agent_id = DB::table('players')
                                        ->where('player_id', $user->id)
                                        ->select('agent_id as a_id')
                                        ->get();

            } elseif($user->hasRole('Agent')) {
                
                $agent_id = DB::table('token_agents')
                                        ->where('token_agent_id', $user->id)
                                        ->select('master_agent_id as a_id')
                                        ->get();

            } elseif($user->hasRole('Master Agent')) {

                $agent_id = DB::table('master_agents')
                                        ->where('master_agent_id', $user->id)
                                        ->select('supreme_agent_id as a_id')
                                        ->get();
            } elseif($user->hasRole('Supreme Agent')) {

                $agent_id = DB::table('supreme_agents')
                                        ->where('supreme_agent_id', $user->id)
                                        ->select('company_user_id as a_id')
                                        ->get();
            } 

            $data = Cashout::create([
                'player_id' => $user->id,
                'agent_id' => $agent_id[0]->a_id,
                'account_name' => $request->account_name,
                'account_number' => $request->account_number,
                'mode_of_payment' => $request->mop,
                'amount' => $request->amount,
                'status' => 'pending'
            ]);


              
   
       
        return $agent_id[0]->a_id;
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cashout  $cashout
     * @return \Illuminate\Http\Response
     */
    public function show(Cashout $cashout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cashout  $cashout
     * @return \Illuminate\Http\Response
     */
    public function edit(Cashout $cashout)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cashout  $cashout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $user = Auth::user();


        $updateCashoutRequest = Cashout::where('id', $request->id)
                                                                        ->update(['status' => 'approved']);

        $updateAgentBalance = PlayerWallet::where('player_id', Auth::user()->id)
                                                        ->increment('available_balance', $request->amount);

        $updatePlayerBalance = PlayerWallet::where('player_id', $request->pid)
                                                            ->decrement('available_balance', $request->amount);


        $running_balance = PlayerWallet::where('player_id', $request->pid)
                                                                    ->select('available_balance')
                                                                    ->get();
        $wallet = Wallet::create([
            'amount' => $request->amount,
            'load_from_user_id' => Auth::user()->id,
            'load_to_user_id' => $request->pid,
            'percentage' => '0',
            'amount_to_be_credited' => $request->amount,
            'transaction_type' => 'cash-out',
            'running_balance' => $running_balance[0]->available_balance,
        ]);
        
   
        
        

        event(new WalletEvent());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cashout  $cashout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cashout $cashout)
    {
        //
    }
}
