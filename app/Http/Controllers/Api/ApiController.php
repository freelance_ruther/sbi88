<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use Illuminate\Http\Response;
use App\Laravue\Models\Permission;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Events;
use App\Bet;
use App\EventDetails;
use App\Wallet;
use App\CashOut;
use App\PlayerWallet;
use App\TokenAgent;
use App\MasterAgent;
use App\SupremeAgent;
use App\Commission;
use App\Outlay;
use App\QuarterScore;
use App\Events\OpenFightEvent;
use App\Events\CloseFightEvent;
use App\Events\EventStarted;
use App\Events\NewBetPlacedEvent;
use App\Events\DeclareWinnerEvent;
use App\Events\CashinEvent;
use App\Events\WalletEvent;
use App\Events\LogoutIndividuallyEvent;
use App\Events\GlobalLogoutEvent;

class ApiController extends BaseController
{
    public function getRoles()
    {
       $roles =  DB::table('roles')
                ->select('name', 'id')
                ->get();

        return $roles;
    }

    public function getCompanyBalance()
    {
       $user = Auth::user();

        if( $user->hasRole('Company') )
        {
            $availableBalance = DB::table('outlays')
                                     ->sum('amount');

            $cashInTotal = DB::table('wallets')
                                            ->where('load_from_user_id', Auth::user()->id)
                                            ->where('transaction_type', 'cash-in')
                                            ->sum('amount_to_be_credited');

            $cashOutTotal = DB::table('wallets')
                                            ->where('load_from_user_id', Auth::user()->id)
                                            ->where('transaction_type', 'cash-out')
                                            ->sum('amount_to_be_credited');
            
            $cashinBalance = $availableBalance - $cashInTotal;
            $cashoutBalance = $cashinBalance + $cashOutTotal;

           
            $balance =  $cashoutBalance;
            
        } elseif ($user->hasRole('Supreme Agent')) {


            // $availableBalance = DB::table('wallets')
            //                                             ->where('load_to_user_id', Auth::user()->id)
            //                                             ->where('transaction_type', 'cash-in')
            //                                             ->sum('amount_to_be_credited');

            // $deductedFromBalance = DB::table('wallets')
            //                         ->where('load_from_user_id', Auth::user()->id)
            //                         ->where('transaction_type', 'cash-in')
            //                         ->sum('amount_to_be_credited');

            // $balance = $availableBalance - $deductedFromBalance;
             
            // $updateWallet = PlayerWallet::where('player_id', Auth::user()->id)
            //                                 ->update(['available_balance' => $balance]);
                                            $newBalance = PlayerWallet::where('player_id', Auth::user()->id)
                                            ->select('available_balance')
                                            ->get();
                                            $balance =  $newBalance[0]->available_balance;
        }  elseif ($user->hasRole('Master Agent')) {


            // $availableBalance = DB::table('wallets')
            //                                             ->where('load_to_user_id', Auth::user()->id)
            //                                             ->where('transaction_type', 'cash-in')
            //                                             ->sum('amount_to_be_credited');

            // $deductedFromBalance = DB::table('wallets')
            //                         ->where('load_from_user_id', Auth::user()->id)
            //                         ->where('transaction_type', 'cash-in')
            //                         ->sum('amount_to_be_credited');

            // $balance = $availableBalance - $deductedFromBalance;
            
            // $updateWallet = PlayerWallet::where('player_id', Auth::user()->id)
            // ->update(['available_balance' => $balance]);
            $newBalance = PlayerWallet::where('player_id', Auth::user()->id)
            ->select('available_balance')
            ->get();
            $balance =  $newBalance[0]->available_balance;
        }  elseif ($user->hasRole('Agent')) {


            // $availableBalance = DB::table('wallets')
            //                                             ->where('load_to_user_id', Auth::user()->id)
            //                                             ->where('transaction_type', 'cash-in')
            //                                             ->orWhere('transaction_type', 'cash-out')
            //                                             ->sum('amount_to_be_credited');

            // $deductedFromBalance = DB::table('wallets')
            //                         ->where('load_from_user_id', Auth::user()->id)
            //                         ->where('transaction_type', 'cash-in')
            //                         ->orWhere('transaction_type', 'cash-out')
            //                         ->sum('amount_to_be_credited');

            // $balance = $availableBalance - $deductedFromBalance;
            //  $updateWallet = PlayerWallet::where('player_id', Auth::user()->id)
            //                                 ->update(['available_balance' => $balance]);
                                          
                                            $newBalance = PlayerWallet::where('player_id', Auth::user()->id)
                                            ->select('available_balance')
                                            ->get(); 
                                            $balance =  $newBalance[0]->available_balance;
        } 
        elseif ($user->hasRole('Player')) {


            // $getWinnings = DB::table('bets')
            //                                 ->where

                                    $newBalance = PlayerWallet::where('player_id', Auth::user()->id)
                                                                        ->select('available_balance')
                                                                        ->get();

                                    $balance =  $newBalance[0]->available_balance;
        } 


        
       
        return $balance;
    }

    public function getUsersForWallet()
    {
         $user = Auth::user();
        
        if( $user->hasRole('Company') )  {
            $u =DB::table('users as u')
                         ->join('supreme_agents as ta', 'ta.supreme_agent_id', '=', 'u.id')
                         ->where('ta.company_user_id', Auth::user()->id)
                         ->where('u.status', 'active')
                         ->select('ta.*', 'u.*')
                         ->get();
        } elseif ( $user->hasRole('Supreme Agent') ) {
            $u =DB::table('users as u')
                                ->join('master_agents as ta', 'ta.master_agent_id', '=', 'u.id')
                                ->where('ta.supreme_agent_id', Auth::user()->id)
                                ->where('u.status', 'active')
                                ->select('ta.*', 'u.*')
                                ->get();
        } elseif ( $user->hasRole('Master Agent') ) {
            $u =DB::table('users as u')
                         ->join('token_agents as ta', 'ta.token_agent_id', '=', 'u.id')
                         ->where('ta.master_agent_id', Auth::user()->id)
                         ->where('u.status', 'active')
                         ->select('ta.*', 'u.*')
                         ->get();
        } elseif( $user->hasRole('Agent') ) {
	    $u = DB::table('users as u')
                    ->join('players as p', 'p.player_id', '=', 'u.id')
                    ->where('p.agent_id', Auth::user()->id)
                    ->where('u.status', 'active')
                    ->select('p.*', 'u.*')
                    ->get();
	}

        return $u;
    }

    public function getReferralCode()
    {
        $refferalCode = DB::table('users')
                                        ->where('id', Auth::user()->id)
                                        ->select('referral_link')
                                        ->get();
        
        return $refferalCode;
    }

    public function getForApprovalPlayers()
    {
        $user = Auth::user();
        $forApproval;

        if($user->hasRole('Company')) {

            $forApproval = DB::table('users as u')
                        ->join('supreme_agents as p', 'p.supreme_agent_id', '=', 'u.id')
                        ->where('p.company_user_id', $user->id)
                        ->where('u.status', 'pending')
                        ->select('u.*', 'p.supreme_agent_id as pid')
                        ->get();

        } elseif($user->hasRole('Supreme Agent')) {

            $forApproval = DB::table('users as u')
                        ->join('master_agents as p', 'p.master_agent_id', '=', 'u.id')
                        ->where('p.supreme_agent_id', $user->id)
                        ->where('u.status', 'pending')
                        ->select('u.*', 'p.master_agent_id as pid')
                        ->get();

        } elseif($user->hasRole('Master Agent')) {

            $forApproval = DB::table('users as u')
                                        ->join('token_agents as p', 'p.token_agent_id', '=', 'u.id')
                                        ->where('p.master_agent_id', $user->id)
                                        ->where('u.status', 'pending')
                                        ->select('u.*', 'p.token_agent_id as pid')
                                        ->get();

        } elseif($user->hasRole('Agent')) {
            $forApproval = DB::table('users as u')
                                        ->join('players as p', 'p.player_id', '=', 'u.id')
                                        ->where('p.agent_id', $user->id)
                                        ->where('u.status', 'pending')
                                        ->select('u.*', 'p.player_id as pid')
                                        ->get();
        }
       

        return $forApproval;
    }

    public function approvePlayer(Request $request)
    {
        $approval = DB::table('users')
                                    ->where('id', $request->id)
                                    ->update(['status' => 'active']);

        return $approval;
    }
    public function getActivePlayers()
    {

        $user = Auth::user();

        if($user->hasRole('Company')) {
            $forApproval = DB::table('users as u')
                                            ->join('supreme_agents as p', 'p.supreme_agent_id', '=', 'u.id')
                                            ->where('p.company_user_id', Auth::user()->id)
                                            ->where('u.status', 'active')
                                            ->select('u.*', 'p.supreme_agent_id as pid')
                                            ->get();
        } elseif($user->hasRole('Supreme Agent')) {

            $forApproval = DB::table('users as u')
                                            ->join('master_agents as p', 'p.master_agent_id', '=', 'u.id')
                                            ->where('p.supreme_agent_id', Auth::user()->id)
                                            ->where('u.status', 'active')
                                            ->select('u.*', 'p.master_agent_id as pid')
                                            ->get();

        } elseif($user->hasRole('Master Agent')) {

            $forApproval = DB::table('users as u')
                                            ->join('token_agents as p', 'p.token_agent_id', '=', 'u.id')
                                            ->where('p.master_agent_id', Auth::user()->id)
                                            ->where('u.status', 'active')
                                            ->select('u.*', 'p.token_agent_id as pid')
                                            ->get();

        } elseif($user->hasRole('Agent')) {

            $forApproval = DB::table('users as u')
                                        ->join('players as p', 'p.player_id', '=', 'u.id')
                                        ->where('p.agent_id', Auth::user()->id)
                                        ->where('u.status', 'active')
                                        ->select('u.*', 'p.player_id as pid')
                                        ->get();
        }
        

        return $forApproval;
    }

    public function deactivatePlayer(Request $request)
    {
        
        $approval = DB::table('users')
                                    ->where('id', $request->id)
                                    ->update(['status' => 'deactivated']);

        return $approval;
    }
    public function getDeactivatedPlayers()
    {
        $user = Auth::user();

        if($user->hasRole('Company')) {
            $deactivatedPlayers = DB::table('users as u')
                                            ->join('supreme_agents as p', 'p.supreme_agent_id', '=', 'u.id')
                                            ->where('p.company_user_id', Auth::user()->id)
                                            ->where('u.status', 'deactivated')
                                            ->select('u.*', 'p.supreme_agent_id as pid')
                                            ->get();
        } elseif($user->hasRole('Supreme Agent')) {

            $deactivatedPlayers = DB::table('users as u')
                                            ->join('master_agents as p', 'p.master_agent_id', '=', 'u.id')
                                            ->where('p.supreme_agent_id', Auth::user()->id)
                                            ->where('u.status', 'deactivated')
                                            ->select('u.*', 'p.master_agent_id as pid')
                                            ->get();

        } elseif($user->hasRole('Master Agent')) {

            $deactivatedPlayers = DB::table('users as u')
                                            ->join('token_agents as p', 'p.token_agent_id', '=', 'u.id')
                                            ->where('p.master_agent_id', Auth::user()->id)
                                            ->where('u.status', 'deactivated')
                                            ->select('u.*', 'p.token_agent_id as pid')
                                            ->get();

        } elseif($user->hasRole('Agent')) {

            $deactivatedPlayers = DB::table('users as u')
                                        ->join('players as p', 'p.player_id', '=', 'u.id')
                                        ->where('p.agent_id', Auth::user()->id)
                                        ->where('u.status', 'deactivated')
                                        ->select('u.*', 'p.player_id as pid')
                                        ->get();
        }
        

        return $deactivatedPlayers;
    }

    public function activatePlayer(Request $request)
    {
        $approval = DB::table('users')
                                    ->where('id', $request->id)
                                    ->update(['status' => 'active', 'active_status' => 'offline']);

        return $approval;
    }

    public function checkIfPlayerIsActive()
    {
       $status = DB::table('users')
                            ->where('id', Auth::user()->id)
                            ->select('status');

        var_dump($status);
    }

    public function startEvent(Request $request)
    {
        if(EventDetails::where('event_id', $request->id)->exists()) {
            return "Already created.";
        } else {
            $startEvent = Events::where('id', $request->id)
            ->where('status', 'upcoming')
            ->update(['status' => 'on-going']);
            for($i=0; $i < $request->fights; $i++) {
                $eventDetails = EventDetails::firstOrCreate([
                    'event_id' => $request->id,
                    'fight_number' => $i+1,
                    'meron_total_bet' => 0.00,
                    'wala_total_bet' => 0.00,
                    'meron_total_payout' => 0.00,
                    'wala_total_payout' => 0.00,
                    'total_bet' => 0.00,
                    'total_commission' => 0.00,
                    'is_winner' => 'tbd',
                    'status' => 'pending',
                    'meron_score' => '0',
                    'wala_score' => '0'
                ]);
            }

            $event = Events::find($request->id);

            broadcast(new EventStarted($event))->toOthers();

            return $event;
        }
      

        

     

        

       

        
    }

    public function closeEvent(Request $request) 
    {
        $closeEvent = Events::where('id', $request->id)
                                            ->where('status', 'on-going')
                                            ->update(['status' => 'finished']);
        return $closeEvent;
    }

    public function getEventDetails($id)
    {
        $eventDetails = EventDetails::where('event_id', $id)->get();
        
        return $eventDetails;
    }

    public function openBet(Request $request)
    {
     
        $eventDetails = DB::table('events as e')
                                         ->join('event_details as ed', 'ed.event_id', '=', 'e.id')
                                         ->where('ed.status', 'open')
                                         ->where('e.id', $request->event_id)
                                         ->select('e.*', 'ed.*')
                                         ->get();
        $openBet = EventDetails::where('event_id', $request->event_id)
                                            ->where('fight_number', $request->fight_number)
                                            ->update(['status' => 'open']);

        $fight = EventDetails::find($request->details_id);
                                           
        broadcast(new OpenFightEvent($fight))->toOthers();

        return $fight;
    }

    public function closeBet(Request $request)
    {
    

        $closeBet = EventDetails::where('event_id', $request->event_id)
                                            ->where('fight_number', $request->fight_number)
                                            ->update(['status' => 'closed']);

        $fight = EventDetails::find($request->details_id);
                                           
        broadcast(new OpenFightEvent($fight))->toOthers();
        return $fight;
    }

    public function getOpenBet()
    {
        $openBet = DB::table('events as e')
                                    ->join('event_details as ed', 'ed.event_id', '=', 'e.id')
                                    ->where('ed.status', 'open')
                                    ->select('e.*', 'ed.*')
                                    ->get();

        return $openBet;

        // return session('event_id');
    }

    public function getFightStatus(Request $request)
    {
        $fightStatus = DB::table('event_details')
                                        ->where('event_id', $request->event_id)
                                        ->where('fight_number', $request->fight_id)
                                        ->get();

        return $fightStatus;
    }

    public function placeBet(Request $request)
    {

        $balance = DB::table('player_wallets')
                                ->where('player_id', Auth::user()->id)
                                ->select('available_balance')
                                ->get();

        if($balance[0]->available_balance <= 0  ) {
            return response()->json(new JsonResponse([], 'Insufficient Balance.'), Response::HTTP_UNAUTHORIZED);
        } elseif($request->bet_amount > $balance[0]->available_balance) {
            return response()->json(new JsonResponse([], 'Insufficient Balance.'), Response::HTTP_UNAUTHORIZED);
        } else {

            $placeBet = Bet::create([
                'choice' => $request->choice,
                'bet_amount' =>  $request->bet_amount,
                'player_id' => Auth::user()->id,
                'event_id' => $request->event_id,
                'fight_number' => $request->fight_id,
                'status' => ''
           ]);

           $deductBalance = PlayerWallet::where('player_id', Auth::user()->id)
                                                        ->decrement('available_balance', $request->bet_amount);
    
           $choice = $request->choice;
    
           if($choice == "meron") {
    
            $countMeronBet = DB::table('bets')
                ->where('choice', 'meron')
                ->where('fight_number', $request->fight_id)
                ->where('event_id', $request->event_id)
                ->sum('bet_amount');
    
               
    
                $updateFightDetails = DB::table('event_details')
                                   
                                    ->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])
                                    ->update(['meron_total_bet' => $countMeronBet]);
                
           } elseif($choice == "wala") {
    
            $countWalaBet = DB::table('bets')
                                    ->where('choice', 'wala')
                                    ->where('fight_number', $request->fight_id)
                                    ->where('event_id', $request->event_id)
                                    ->sum('bet_amount');                         
            $updateFightDetails = DB::table('event_details')
                            
                            ->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])
                            ->update(['wala_total_bet' => $countWalaBet]);
           }
    
           /**
            * Compute for payout
            *Get 10% of Total Bet ( meron + wala ): Total Bet = meron + wala, Total Bet * .10 = CutTotalBet
            * Total Bet(w/ 10%) / Total of Meron Bet / Total of Wala Bet: CutTotalBet / meron or wala
            * Quotient of Total Bet(w/ 10%) x 100: QuotientofCutTotalBet x 100
            * 
            */
    
             $getMeronTotal = EventDetails::select('meron_total_bet')->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])->first();
    
            $getWalaTotal = EventDetails::select('wala_total_bet')->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])->first();
    
            $fightTotalBet = $getMeronTotal->meron_total_bet + $getWalaTotal->wala_total_bet;

            $fightTotalCommission = $fightTotalBet * .07;

            $updateTotalBet = EventDetails::where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])
                                                                    ->update(['total_bet' => $fightTotalBet, 'total_commission' => $fightTotalCommission]);
            
    
            $tenPercentOfMeron = $getMeronTotal->meron_total_bet * .05;
            $tenPercentOfWala = $getWalaTotal->wala_total_bet * .05;
    
            $withTenMeron =  $getMeronTotal->meron_total_bet - $tenPercentOfMeron;
             $withTenWala =  $getWalaTotal->wala_total_bet - $tenPercentOfWala;
    
             $totalBet = $withTenMeron + $withTenWala;
    
    
            if($getMeronTotal->meron_total_bet != 0) {
                $quotienForMeron = ($totalBet / $withTenMeron) * 100;
                $deductFiveForMeron = $quotienForMeron * .05;
                $totalPayoutForMeron = $quotienForMeron - $deductFiveForMeron;
                $updatePayout = DB::table('event_details')
                ->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])
                ->update(['meron_total_payout' => $totalPayoutForMeron]);
    
            }
           
            if($getWalaTotal->wala_total_bet != 0) {
                $quotientForWala = ($totalBet / $withTenWala) * 100;
                $deductFiveForWala = $quotientForWala * .05;
                $totalPayoutForWala = $quotientForWala - $deductFiveForWala;
                $updatePayout = DB::table('event_details')
                ->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])
                ->update(['wala_total_payout' => $totalPayoutForWala]);
    
            }
           
            // return $quotienForMeron;
            $eventDetails = EventDetails::where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])->first();
    
            event(new NewBetPlacedEvent($eventDetails));
           event(new WalletEvent());

           return $eventDetails;
        }
     
     
                
                                                

        // return EventDetails::all();

    }

    public function declareWinner(Request $request)
    {
        $name = "";

        if($request->hasFile('files')) {
            $file = $request->file('files');
            $name = time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('posters'), $name);
        }

        $updateEvent = DB::table('events')
                                         ->where('id', $request->event_id)
                                         ->update(['image_path' => $name]);
        


        $updateWinner = EventDetails::where('event_id', $request->event_id)
                                                            ->where('fight_number', $request->fight_number)
                                                            ->update(['status' => 'done', 'is_winner' => $request->winner, 'meron_score' => $request->meron_score, 'wala_score' => $request->wala_score]);

        if($request->winner == "cancelled" || $request->winner == "draw") {
           

            $creditBack = DB::table('bets')
                                        ->where('fight_number', $request->fight_number)
                                        ->where('event_id', $request->event_id)
                                        ->update(['status' => $request->winner ]);

        }  else {
            
            $updateWinner = DB::table('bets')
                                        ->where('fight_number', $request->fight_number)
                                        ->where('choice', $request->winner)
                                        ->where('event_id', $request->event_id)
                                        ->update(['status' => 'win', 'isCredited' => 'no']);

            $updateLoser = DB::table('bets')
                                        ->where('fight_number', $request->fight_number)
                                        ->where('choice', '<>', $request->winner)
                                        ->where('event_id', $request->event_id)
                                        ->update(['status' => 'lose', 'isCredited' => 'yes']);           
        }
    

        $fight = EventDetails::find($request->details_id);
                
        broadcast(new DeclareWinnerEvent($fight))->toOthers();
        return $fight;
    }

    public function checkIfWinner(Request $request) 
    {

        
    
        $newBalance;
        $updated = "";

        $whoIsWinner = DB::table('event_details')
                                        ->where('event_id', $request->event_id)
                                        ->where('fight_number', $request->fight_id)
                                        ->select('is_winner', 'meron_total_payout', 'wala_total_payout')
                                        ->get();

        $addCommissionToOutlay = EventDetails::select('total_commission')
                                                            ->where(['event_id' => $request->event_id, 'fight_number' => $request->fight_id])
                                                            ->first();

        if($whoIsWinner[0]->is_winner == "meron") {
            $payout = $whoIsWinner[0]->meron_total_payout;

            $updateOutlay = Outlay::where('id', 1)
                                                    ->increment('amount', $addCommissionToOutlay->total_commission);

        } elseif($whoIsWinner[0]->is_winner == "wala") {
            $payout = $whoIsWinner[0]->wala_total_payout; 
            $updateOutlay = Outlay::where('id', 1)
                                                    ->increment('amount', $addCommissionToOutlay->total_commission);
        } 

        $check = DB::table('bets')
                        ->where('player_id', Auth::user()->id)
                        ->where('event_id', $request->event_id)
                        ->where('fight_number', $request->fight_id)
                        ->select('status')
                        ->get();

        if($check[0]->status == "win") {

          
            $isCredited = DB::table('bets')
                                            ->where('player_id', Auth::user()->id)
                                            ->where('event_id', $request->event_id)
                                            ->where('fight_number', $request->fight_id)
                                            ->select('isCredited')
                                            ->get();

            
            if($isCredited[0]->isCredited == 'no') {

                $getBet = DB::table('bets')
                                ->where('player_id', Auth::user()->id)
                                ->where('event_id', $request->event_id)
                                ->where('fight_number', $request->fight_id)
                                ->where('status', 'win')
                                ->sum('bet_amount');

            $totalWinnings = ($getBet / 100) * $payout;

            // $newBalance = PlayerWallet::where('player_id', Auth::user()->id)
            //                                                     ->increment('available_balance', $totalWinnings);

            $newBalance = DB::table('player_wallets')
                                                ->where('player_id', Auth::user()->id)
                                                ->update(['available_balance' => DB::raw('available_balance + ' . $totalWinnings)]);
            
            
    
            $updated = PlayerWallet::where('player_id', Auth::user()->id)
                                                        ->get();

            $updateIsCredited = DB::table('bets')
                                                        ->where('player_id', Auth::user()->id)
                                                        ->where('event_id', $request->event_id)
                                                        ->where('fight_number', $request->fight_id)
                                                        ->update(['isCredited'  => 'yes']);
            
            $agent_id = DB::table('players')
                                        ->where('player_id', Auth::user()->id)
                                        ->select('agent_id')
                                        ->get();

            $master_agent_id = DB::table('token_agents')
                                                    ->where('token_agent_id', $agent_id[0]->agent_id)
                                                    ->select('master_agent_id')
                                                    ->get();

            $supreme_agent_id = DB::table('master_agents')
                                                        ->where('master_agent_id', $master_agent_id[0]->master_agent_id)
                                                        ->select('supreme_agent_id')
                                                        ->get();
            
            $commission = $totalWinnings * .01;

            $add_to_commission = Commission::firstOrCreate([
                'supreme_agent_id' => $supreme_agent_id[0]->supreme_agent_id,
                'master_agent_id' => $master_agent_id[0]->master_agent_id,
                'agent_id' => $agent_id[0]->agent_id,
                'player_id' => Auth::user()->id,
                'event_id' => $request->event_id,
                'fight_id' => $request->fight_id,
                'earnings' => $totalWinnings,
                'commission_per_upline' => $commission
            ]);

                $updateSupremeBalance = PlayerWallet::where('player_id', $supreme_agent_id[0]->supreme_agent_id)
                                                                                    ->increment('available_balance', $commission);
                $updateMasterBalance = PlayerWallet::where('player_id', $master_agent_id[0]->master_agent_id)
                                                                                    ->increment('available_balance', $commission);
                $updateAgentBalance = PlayerWallet::where('player_id', $agent_id[0]->agent_id)
                                                                                    ->increment('available_balance', $commission);
                         

                event(new WalletEvent());
                return $updated;
            } 
            

        } elseif($check[0]->status == "cancelled" || $check[0]->status == "draw") {

            $getBet = DB::table('bets')
                                ->where('player_id', Auth::user()->id)
                                ->where('event_id', $request->event_id)
                                ->where('fight_number', $request->fight_id)
                                ->where('status', 'cancelled')
                                ->orWhere('status', 'draw')
                                ->sum('bet_amount');

            $creditBack = PlayerWallet::where('player_id', Auth::user()->id)
                                                            ->increment('available_balance', $getBet);

            event(new WalletEvent());

            return $creditBack;
        }
       

        
        
        //return $whoIsWinner[0]->is_winner;
       
    }

    public function checkUserBalance()
    {
        $balance =   DB::table('player_wallets')
                                    ->where('player_id', Auth::user()->id)
                                    ->select('available_balance')
                                    ->get();

                                 

        return $balance;
    }

    public function getCashInLogs()
    {
        $data = Wallet::where(['load_to_user_id' => Auth::user()->id, 'transaction_type' => 'cash-in'])->get();

        return $data;
    }

    public function getCashOutLogs()
    {
        $data = Wallet::where(['load_to_user_id' => Auth::user()->id, 'transaction_type' => 'cash-out'])->get();

        return $data;
    }

    public function getTotalCommission()
    {
        $user = Auth::user();

        if($user->hasRole('Supreme Agent')) {

            $data = DB::table('commissions')
                                ->where('supreme_agent_id', $user->id)
                                ->sum('commission_per_upline');
                                return $data;

        } elseif ($user->hasRole('Master Agent')) {
            $data = DB::table('commissions')
                                ->where('master_agent_id', $user->id)
                                ->sum('commission_per_upline');
                                return $data;
        } elseif ($user->hasRole('Agent')) {
            $data = DB::table('commissions')
            ->where('agent_id', $user->id)
            ->sum('commission_per_upline');
            return $data;
        }
    }

    public function getAllUserWallet()
    {
        $data = DB::table('player_wallets as w')
                            ->join('users as u', 'u.id', '=', 'w.player_id')
                            ->select('u.name as member', 'w.available_balance as balance')
                            ->get();

        return $data;
    }

    public function getCashoutLogPerUser()
    {

        $user = Auth::user();

        $data = DB::table('cashouts as c')
                    ->join('users as u', 'u.id', '=', 'c.player_id')
                    ->where('player_id', $user->id)
                    ->select('c.*', 'u.name as member_name')
                    ->get();

        return $data;
    }

    public function getGameDetails()
    {
        $data = DB::table('events as e')
                            ->join('event_details as d', 'd.event_id', 'e.id')
                            ->where('e.status', 'on-going')
                            ->where('d.status', 'pending')
                            ->orWhere('d.status', 'open')
                            ->select('e.name', 'e.stream_url', 'd.*')
                            ->get();

        return $data;
    }

    public function  getStreamUrl()
    {
        $data = DB::table('events')
                            ->select('stream_url', 'event_type', 'image_path')
                            ->where('status', 'on-going')
                            ->get();

        return $data;
    }

    public function unlockAccount(Request $request)
    {
        $unlock = User::where('id', $request->id)
                            ->update(['active_status' => 'offline']);

        broadcast(new LogoutIndividuallyEvent(User::find($request->id)));
        return $unlock;
    }

    public function updateScoreBoard(Request $request)
    {
        if($request->hasFile('files')) {
            $file = $request->file('files');
            $name = time().'.'.$file->getClientOriginalExtension();
            $file->move(public_path('posters'), $name);
        }
    }

    public function getScores(Request $request)
    {
        $scores = EventDetails::where('event_id', $request->event_id)
                                                
                                                ->select('meron_score', 'wala_score')
                                                ->get();

        return $scores;
    }

    public function unlockAllAccount()
    {
     

            $unlock = DB::table('users')
                                ->where('active_status', 'online')
                                ->where('id', '<>', 1)
                                ->update(['active_status' => 'offline']);
                                broadcast(new GlobalLogoutEvent())->toOthers();
            return "ok";
     
        
    }

    public function unlockAdmin()
    {
        $unlock = DB::table('users')
                                ->where('id', 1)
                                ->update(['active_status' => 'offline']);
    }
   
}
