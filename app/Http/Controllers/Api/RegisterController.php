<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use DB;

use App\Http\Resources\PermissionResource;
use App\Http\Resources\UserResource;
use App\Laravue\JsonResponse;
use App\Laravue\Models\Permission;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\MasterAgent;
use App\TokenAgent;
use App\Player;
use App\SupremeAgent;
use App\PlayerWallet;


class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($code)
    {
      
       if( User::where('referral_link', $code)->count() > 0 ) {
          return view('register');
       } 
                                                

    }
    
    public function supremeAgent($code)
    {
        if( User::where('referral_link', $code)->count() > 0 ) {
            return view('supreme-agent');
         } 
    }

    public function masterAgent($code)
    {
        if( User::where('referral_link', $code)->count() > 0 ) {
            return view('master-agent');
         } 
    }

    public function agent($code)
    {
        if( User::where('referral_link', $code)->count() > 0 ) {
            return view('agent');
         } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array_merge(
                $this->getValidationRules(),
                [
                    'password' => ['required', 'min:6'],
                    'confirmPassword' => 'same:password',
                ]
            )
        );

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 403);
        } else {

            $params = $request->all();

            // Generate referral code
            // Will be attached to referral link
            $ref = Str::random(5);

            $user = User::create([
                'name' => $params['name'],
                'email' => $params['email'],
                'phone_number' => $params['phone_number'],
                'password' => Hash::make($params['password']),
                'referral_link' => $ref,
                'status' => 'pending',
                'active_status' => 'offline'
            ]);

            $wallet = PlayerWallet::create([
                'player_id' => $user->id,
                'available_balance' => '0',
            ]);

            $role = Role::findByName($params['role']);
            $user->syncRoles($role);

            if($params['role'] == 'Master Agent') {

                $id = DB::table('users')
                                ->where('referral_link', $params['ref_code'])
                                ->select('id')
                                ->get();

                    $masterAgent = MasterAgent::create([
                        'supreme_agent_id' => $id[0]->id,
                        'master_agent_id' => $user->id,
                    ]);
                    
                  

            }  elseif($params['role'] == 'Agent') {
                
                $id = DB::table('users')
                                ->where('referral_link', $params['ref_code'])
                                ->select('id')
                                ->get();

                $tokenAgent = TokenAgent::create([
                    'master_agent_id' =>$id[0]->id,
                    'token_agent_id' => $user->id,
                  
                ]);

            }   elseif($params['role'] == 'Supreme Agent') {
                
                $id = DB::table('users')
                            ->where('referral_link', 'MYM21')
                            ->select('id')
                            ->get();

                $suprementAgent = SupremeAgent::create([
                    'company_user_id' =>$id[0]->id,
                    'supreme_agent_id' => $user->id,
                  
                ]);

            } elseif($params['role'] == 'Player') {

                $id = DB::table('users')
                                ->where('referral_link', $params['ref_code'])
                                ->select('id')
                                ->get();

                $player = Player::create([
                    'agent_id' => $id[0]->id,
                    'player_id' => $user->id,
                    'status' => 'pending',
                ]);
                
            } 
           
            

            return $user->id;
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function getValidationRules($isNew = true)
    {
        return [
            'name' => 'required|unique:users',
            'email' => $isNew ? 'required|unique:users' : 'required',
            'phone_number' => 'required|unique:users',
            'roles' => [
                'required',
                'array'
            ],
          
            
        ];
    }
}
