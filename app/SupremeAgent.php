<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupremeAgent extends Model
{
    protected $fillable = [
        'company_user_id', 'supreme_agent_id'
    ];
}
