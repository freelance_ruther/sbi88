<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerWallet extends Model
{
    protected $fillable = [
        'player_id', 'available_balance'  
    ];
}
