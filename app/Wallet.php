<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = [
        'amount', 'load_from_user_id', 'load_to_user_id', 'percentage', 'amount_to_be_credited', 'transaction_type', 'running_balance'
    ];
}
