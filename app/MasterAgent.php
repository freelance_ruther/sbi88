<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterAgent extends Model
{
    protected $table = 'master_agents';
    
    protected $fillable = [
        'supreme_agent_id', 'master_agent_id',
    ];
}
