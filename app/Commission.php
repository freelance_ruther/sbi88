<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    protected $fillable = [
        'supreme_agent_id', 'master_agent_id', 'agent_id', 'player_id', 'event_id', 'fight_id', 'earnings', 'commission_per_upline'
    ];
}
