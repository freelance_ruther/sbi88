<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Percentage extends Model
{
    protected $fillable = [
        'percentage_cut', 'role_id'
    ];
}
