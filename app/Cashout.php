<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cashout extends Model
{
    protected $fillable = [
        'player_id', 'agent_id', 'account_name', 'account_number', 'mode_of_payment', 'status', 'amount'
    ];
}
