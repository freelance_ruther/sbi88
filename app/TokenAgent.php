<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TokenAgent extends Model
{
    protected $fillable = [
        'supreme_agent_id', 'master_agent_id', 'token_agent_id'
    ];
}
