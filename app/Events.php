<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = [
        'name', 'start_date', 'start_time', 'total_budget', 'total_cash_in', 'total_cash_out', 'running_balance', 'number_of_fights', 'status', 'stream_url',
        'event_type', 'image_path',
    ];
}
