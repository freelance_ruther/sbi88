<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bet extends Model
{
    protected $fillable = [
        'player_id', 'choice', 'bet_amount', 'fight_number', 'status', 'event_id', 'isCredited', 'total_winnings'
    ];
}
