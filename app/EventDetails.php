<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventDetails extends Model
{
    protected $fillable = [
        'event_id', 'fight_number', 'meron_total_bet', 'wala_total_bet', 'meron_total_payout',
        'wala_total_payout', 'is_winner', 'status', 'total_bet', 'total_commission', 'meron_score', 'wala_score'
    ];

    
}
