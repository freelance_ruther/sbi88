<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title Page-->
    <title>SBI888 Registration</title>

    <!-- Icons font CSS-->
    <link href="{{ asset('vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="{{ asset('vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('vendor/datepicker/daterangepicker.css') }}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{{ asset('vendor/main.css') }}" rel="stylesheet" media="all">
</head>

<body>
    <div class="page-wrapper bg-gra-01 p-t-50 p-b-100 font-poppins">
   
        <div class="wrapper wrapper--w780">
            <div class="card card-3">
                <div class="card-heading">
                 
                </div>
                <div class="card-body">
                    <span class="valid-form" style="color: #57b846;">Congratulations! Your account has been created. Waiting for your Agent Approval.</span>
                   <form  id="formSubmit">
                   {{ csrf_field() }} 
                        <div class="input-group">
                            <input class="input--style-3" class="form-control" type="text" placeholder="Name" name="name">
                            <span class="invalid-feedback" style="color: #dc3545;"> </span>
                        </div>
                        <div class="input-group">
                          <input class="input--style-3" type="text" placeholder="Phone Number" name="phone_number">
                          <span class="invalid-feedback" style="color: #dc3545;"> </span>
                        </div>
                        <div class="input-group">
                         <input class="input--style-3" type="text" placeholder="Username" name="email"> 
                         <span class="invalid-feedback" style="color: #dc3545;"> </span>
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="password" placeholder="Password" name="password">
                        </div>
                        <div class="input-group">
                            <input class="input--style-3" type="password" placeholder="Confirm Password" name="confirmPassword">
                            <span class="invalid-feedback" style="color: #dc3545;"> </span>
                        </div>
                        <input type="hidden" name="roles[]" value="Supreme Agent">
                        <input type="hidden" name="role" value="Supreme Agent">
                        <input type="hidden" name="ref_code" value="{{ request()->code }}">
                        <div class="p-t-10">
                            <button class="btn btn--pill btn--green submit-btn" type="submit">Submit</button>
                            <a href="/" style="color: #fff; margin-left: 10px;">or Sign In Here</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <!-- Vendor JS-->
    <script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/datepicker/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/datepicker/daterangepicker.js') }}"></script>

    <!-- Main JS-->
    <script src="{{ asset('js/global.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });

        $(".valid-form").hide();
        $(document).ready(function () {
            $("#formSubmit").submit(function(e) {
                e.preventDefault();
                $(document).find("span.invalid-feedback").remove();
                let formData = $("#formSubmit").serialize();
                $.ajax({
                    type: 'POST',
                    url: '/r/store',
                    data: formData,
                    success: function(response) {
                        $(".valid-form").show();
                    },
                    error: function(response) {
                        $.each(response.responseJSON.errors, function(fields, error) {
                            $(document).find('[name='+fields+']').after('<span class="invalid-feedback" style="color:#dc3545">' +error+ '</span>')
                        })
                    }
                })
            });
        });
    </script>
</body>

</html>
<!-- end document-->