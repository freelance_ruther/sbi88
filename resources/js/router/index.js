import Vue from 'vue';
import Router from 'vue-router';

/**
 * Layzloading will create many files and slow on compiling, so best not to use lazyloading on devlopment.
 * The syntax is lazyloading, but we convert to proper require() with babel-plugin-syntax-dynamic-import
 * @see https://doc.laravue.dev/guide/advanced/lazy-loading.html
 */

Vue.use(Router);

/* Layout */
import Layout from '@/layout';

/* Router for modules */
import adminRoutes from './modules/admin';

/**
 * Sub-menu only appear when children.length>=1
 * @see https://doc.laravue.dev/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin', 'editor']   Visible for these roles only
    permissions: ['view menu zip', 'manage user'] Visible for these permissions only
    title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb (default is true)
    affix: true                  if true, the tag will affix in the tags-view
  }
**/

export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/AuthRedirect'),
    hidden: true,
  },
  {
    path: '/404',
    redirect: { name: 'Page404' },
    component: () => import('@/views/error-page/404'),
    hidden: true,
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true,
  },
  {
    path: '',
    component: Layout,
    redirect: 'dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/Index'),
        name: 'Dashboard',
        meta: { title: 'dashboard', icon: 'el-icon-menu', noCache: true },
      },
    ],
  },
 {
  path: '/overview',
  component: Layout,
  redirect: '/overview',
  children: [
    {
      path: '/overview',
      component: () => import('@/views/dashboard/Overview'),
      name: 'Play',
      meta: { title: 'Play', icon: 'play', noCache: true },
    },
  ],
},
  
];

export const asyncRoutes = [
  adminRoutes,
  
  {
    path: '/outlay',
    component: Layout,
    redirect: '/outlay/Index',
    name: 'Cash Outlay',
    alwaysShow: false,
    meta: {
      title: 'cashOutlay', // This title will show on the breadcrumb before submenu's title
      icon: 'example', // Use star icon
      roles: ['Company'],
      permissions: ['view menu administrator'],
    },
    children: [
      {
        path: 'Index', // When clicking on this menu, it will redirect to /#/foo/Index
        name: 'cashOutlay',
        component: () => import('@/views/outlay/Index'),
        meta: { title: 'Cash Outlay', noCache: true }, // Show `foo` on the sidebar
        roles: ['Company'],
        permissions: ['view menu administrator'],
      },
    ],
  },
  {
    path: '/events',
    component: Layout,
    redirect: '/events/Index',
    name: 'Events',
    alwaysShow: false,
    meta: {
      title: 'Events', // This title will show on the breadcrumb before submenu's title
      icon: 'el-icon-date', // Use star ico
      roles: ['Company', 'Mediator'],
    },
    children: [
      {
        path: 'Index',
        name: 'events',
        component: () => import('@/views/events/Index'),
        meta: { title: 'Events', icon: 'el-icon-date', noCache: true }, // Show `foo` on the sidebar
      },
    ],
  },
 
  {
    path: '/wallet',
    component: Layout,
    redirect: '/wallet/Index',
    name: 'Wallet',
    alwaysShow: false,
    
    meta: {
      title: 'Wallet', // This title will show on the breadcrumb before submenu's title
      icon: 'wallet', // Use star ico
      roles: ['Company', 'Supreme Agent', 'Master Agent', 'Agent'],
    },
    children: [
      {
        path: 'Index',
        name: 'walletStation',
        component: () => import('@/views/wallet/Index'),
        meta: { title: 'Wallet Station', icon: 'wallets', noCache: true }, // Show `foo` on the sidebar
        
       
      },
      {
        path: 'cashout', // When clicking on this menu, it will redirect to /#/foo/Index
        name: 'cashoutRequest',
        component: () => import('@/views/wallet/Cashout'),
        meta: { title: 'Cashout Request', icon: 'nested' }, // Show `foo` on the sidebar
       
      },
      {
        path: 'user-wallet', // When clicking on this menu, it will redirect to /#/foo/Index
        name: 'userWallet',
        component: () => import('@/views/wallet/UserWallet'),
        meta: { title: 'User Wallet', icon: 'nested', roles: ['Company'] }, // Show `foo` on the sidebar
       
      },
    ],
  },
  {
    path: '/manage-agents',
    component: Layout,
    redirect: 'agents/master-agents',
    name: 'Agents',
    alwayShow: false,
    meta: {
      title: 'Manage Agents',
      icon: 'people',
      roles: ['Supreme Agent', 'Master Agent']
    },
    children: [
      {
        path: 'master-agents',
        name: 'MasterAgents',
        meta: { title: 'Master Agents', icon: 'people',    roles: ['Supreme Agent'], noCache: true },
     
        component: () => import('@/views/master_agent/Index'),
      },
      {
        path: 'token-agents',
        name: 'TokenAgents',
        meta: { title: 'Token Agents', icon: 'people',  roles: ['Master Agent'], noCache: true },
       
        component: () => import('@/views/token-agent/Index'),
      },
    ],
  },
  {
    path: '/manage-players',
    component: Layout,
    redirect: '/manage-players',
    name: 'Players',
    alwayShow: false,
    meta: {
      title: 'Manage Players',
      icon: 'component',
      roles: ['Agent','Company', 'Supreme Agent', 'Master Agent'],
    },
    children: [
      {
        path: 'active-players',
        name: 'ActivePlayers',
        meta: { title: 'Active', icon: 'people', noCache: true },
        component: () => import('@/views/manage-players/Active'),
      },
      {
        path: 'for-approval',
        name: 'ForApprovalPlayers',
        meta: { title: 'For Approval', icon: 'create-user' , noCache: true},
        component: () => import('@/views/manage-players/Approval'),
      },
      {
        path: 'deactivated-players',
        name: 'DeactivatedPlayers',
        meta: { title: 'Deactivated', icon: 'el-icon-remove-outline', noCache: true },
        component: () => import('@/views/manage-players/Deactivated'),
      },
    ],
  },

  {
    path: '/facilitate',
    component: Layout,
    redirect: 'facilitate/index',
    name: 'Commissions',
    alwayShow: false,
    meta: {
      title: 'Facilitate',
      icon: 'nested',
      roles: ['Mediator'],
    },
    children: [
      {
        path: 'index',
        name: 'Facilitate',
        meta: { title: 'Facilitate', icon: 'nested', noCache: true },
        component: () => import('@/views/facilitate/Index'),
      },
      {
        path: 'edit/:id(\\d+)',
        component: () => import('@/views/facilitate/FacilitateEvent'),
        props:true,
        name: 'FacilitateEvent',
        meta: { title: 'facilitateEvent', noCache: true },
        hidden: true,
      },
    ],
  },
  {
    path: '/credits',
    component: Layout,
    redirect: 'credits',
    name: 'Credits History',
    alwaysShow: false,
    
    meta: {
      title: 'Credits History', // This title will show on the breadcrumb before submenu's title
      icon: 'dollar', // Use star ico
      roles: ['Player', 'Agent', 'Master Agent', 'Supreme Agent'],
    },
    children: [
      {
        path: 'cashin',
        name: 'cashIn',
        component: () => import('@/views/cashin/Index'),
        meta: { title: 'Cash In', icon: 'dollar', noCache: true }, // Show `foo` on the sidebar
        
       
      },
      {
        path: 'cashout', // When clicking on this menu, it will redirect to /#/foo/Index
        name: 'cashout',
        component: () => import('@/views/cashout/Index'),
        meta: { title: 'Cash Out', icon: 'nested' }, // Show `foo` on the sidebar
       
      },
    ],
  },

  {
    path: '/commissions',
    component: Layout,
    redirect: 'commission/index',
    name: 'Commissions',
   
    
    meta: {
      title: 'Commissions', // This title will show on the breadcrumb before submenu's title
      icon: 'wallets', // Use star ico
      roles:['Company', 'Supreme Agent' ,'Master Agent', 'Agent'],
    },
    children: [
      {
        path: 'index',
        name: 'commissions',
        component: () => import('@/views/commission/Index'),
        meta: { title: 'Company Commissions', icon: 'dollar', noCache: true,roles:['Company'], }, // Show `foo` on the sidebar
        
       
      },
      {
        path: 'agent-commissions',
        name: 'AgentCommissions',
        meta: { title: 'Agent Commissions', icon: 'dollar', noCache: true, roles:['Supreme Agent' ,'Master Agent', 'Agent'], },
        component: () => import('@/views/commission/AgentCommission'),
      },
    ],
  
  },


];

const createRouter = () => new Router({
//  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  base: process.env.MIX_LARAVUE_PATH,
  routes: constantRoutes,
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
