import Layout from '@/layout';

const supremeAgentRoutes = [
  {
    path: '/manage-agents',
    component: Layout,
    redirect: 'suprement-agent/manage-agents',
    name: 'Agents',
    alwayShow: false,
    meta: {
      title: 'Manage Agents',
      icon: 'people',
      roles: ['Supreme Agent'],
    },
    children: [
      {
        path: 'master-agents',
        name: 'MasterAgents',
        meta: { title: 'Master Agents', icon: 'people' },
        component: () => import('@/views/dashboard/index'),
      },
      {
        path: 'token-agents',
        name: 'TokenAgents',
        meta: { title: 'Token Agents', icon: 'people' },
        component: () => import('@/views/dashboard/index'),
      },
    ],
  },
];

export default supremeAgentRoutes;
