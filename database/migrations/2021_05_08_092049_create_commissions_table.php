<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->id();
            $table->integer('supreme_agent_id');
            $table->integer('master_agent_id');
            $table->integer('agent_id');
            $table->integer('player_id');
            $table->integer('event_id');
            $table->integer('fight_id');
            $table->float('earnings', 20,2);
            $table->float('commission_per_upline', 20,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
