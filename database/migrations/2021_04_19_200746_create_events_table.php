<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('start_date');
            $table->string('start_time');
            $table->string('stream_url')->nullable();
            $table->float('total_budget', 10,2);
            $table->float('total_cash_in', 10,2)->nullable();
            $table->float('total_cash_out', 10,2)->nullable();
            $table->float('running_balance', 10,2)->nullable();
            $table->integer('number_of_fights')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
