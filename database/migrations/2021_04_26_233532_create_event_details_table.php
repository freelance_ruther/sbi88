<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_details', function (Blueprint $table) {
            $table->id();
            $table->integer('event_id');
            $table->integer('fight_number');
            $table->float('meron_total_bet', 20,2);
            $table->float('wala_total_bet', 20,2);
            $table->float('meron_total_payout', 20,2);
            $table->float('wala_total_payout', 20,2);
            $table->string('is_winner');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_details');
    }
}
