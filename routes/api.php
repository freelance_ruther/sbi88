<?php

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Laravue\Faker;
use \App\Laravue\JsonResponse;
use \App\Laravue\Acl;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function() {
    Route::post('auth/login', 'AuthController@login');
    Route::group(['middleware' => 'auth:sanctum'], function () {
        // Auth routes
        // Route::get('auth/user', 'AuthController@user');
        Route::post('auth/logout', 'AuthController@logout');

        Route::get('/user', function (Request $request) {
          
            return new UserResource($request->user());
        });

        // Api resource routes
        Route::apiResource('roles', 'RoleController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::apiResource('users', 'UserController');
        Route::apiResource('permissions', 'PermissionController')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);

        // Custom Api resource routes
        Route::apiResource('events', 'EventController');
        Route::apiResource('outlays', 'OutlayController')->middleware('permission:' .ACL::PERMISSION_PERMISSION_MANAGE);
        Route::apiResource('wallets', 'WalletController');
        Route::apiResource('master_agents', 'MasterAgentController');
        Route::apiResource('token_agents', 'TokenAgentController');
        Route::apiResource('commissions', 'CommissionController');
        // Custom routes
        Route::put('users/{user}', 'UserController@update');
        Route::get('users/{user}/permissions', 'UserController@permissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);
        Route::put('users/{user}/permissions', 'UserController@updatePermissions')->middleware('permission:' .Acl::PERMISSION_PERMISSION_MANAGE);
        Route::get('roles/{role}/permissions', 'RoleController@permissions')->middleware('permission:' . Acl::PERMISSION_PERMISSION_MANAGE);

        // Defined routes
        Route::get('get_roles', 'ApiController@getRoles');
        Route::get('get_percentage', 'PercentageController@index');
        Route::get('get_company_balance', 'ApiController@getCompanyBalance');
        Route::get('get_users_for_wallet', 'ApiController@getUsersForWallet');
        Route::get('get_referral_code', 'ApiController@getReferralCode');
        Route::get('get_for_approval_players', 'ApiController@getForApprovalPlayers');
        Route::post('approve_player', 'ApiController@approvePlayer');
        Route::get('get_active_players', 'ApiController@getActivePlayers');
        Route::post('deactivate_player', 'ApiController@deactivatePlayer');
        Route::get('get_deactivated_players', 'ApiController@getDeactivatedPlayers');
        Route::post('activate_player', 'ApiController@activatePlayer');
        Route::get('check_if_player_is_active', 'ApiController@checkIfPlayerIsActive');
        Route::post('start_event', 'ApiController@startEvent');
        Route::get('get_event_details/{id}', 'ApiController@getEventDetails');
        Route::post('open_bet', 'ApiController@openBet');
        Route::get('get_open_bet', 'ApiController@getOpenBet');
        Route::post('close_bet', 'ApiController@closeBet');
        Route::post('get_fight_status', 'ApiController@getFightStatus');
        Route::post('place_bet', 'ApiController@placeBet');
        Route::get('check_user_balance', 'ApiController@checkUserBalance');
        Route::post('declare_winner', 'ApiController@declareWinner');
        Route::post('check_if_winner', 'ApiController@checkIfWinner');
        Route::get('get_cash_in_logs', 'ApiController@getCashInLogs');
        Route::get('get_cash_out_logs', 'CashoutController@index');
        Route::post('cashout_store', 'CashoutController@store');
        Route::post('cashout_update', 'CashoutController@update');
        Route::get('get_total_commission', 'ApiController@getTotalCommission');
        Route::get('get_all_user_wallet', 'ApiController@getAllUserWallet');
        Route::get('get_cashout_log_per_user', 'ApiController@getCashoutLogPerUser');
        Route::post('unlock_account', 'ApiController@unlockAccount');
        Route::post('close_event', 'ApiController@closeEvent');
        Route::post('update_score_board', 'ApiController@updateScoreBoard');
        Route::post('get_scores', 'ApiController@getScores');
        // Game Details
        Route::get('get_game_details', 'ApiController@getGameDetails');
        Route::get('get_stream_url', 'ApiController@getStreamUrl');
        Route::get('unlock_all_account', 'ApiController@unlockAllAccount');
    });
});

// Fake APIs
Route::get('/table/list', function () {
    $rowsNumber = mt_rand(20, 30);
    $data = [];
    for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
        $row = [
            'author' => Faker::randomString(mt_rand(5, 10)),
            'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
            'id' => mt_rand(100000, 100000000),
            'pageviews' => mt_rand(100, 10000),
            'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
            'title' => Faker::randomString(mt_rand(20, 50)),
        ];

        $data[] = $row;
    }

    return response()->json(new JsonResponse(['items' => $data]));
});

Route::get('/orders', function () {
    $rowsNumber = 8;
    $data = [];
    for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
        $row = [
            'order_no' => 'LARAVUE' . mt_rand(1000000, 9999999),
            'price' => mt_rand(10000, 999999),
            'status' => Faker::randomInArray(['success', 'pending']),
        ];

        $data[] = $row;
    }

    return response()->json(new JsonResponse(['items' => $data]));
});

Route::get('/articles', function () {
    $rowsNumber = 10;
    $data = [];
    for ($rowIndex = 0; $rowIndex < $rowsNumber; $rowIndex++) {
        $row = [
            'id' => mt_rand(100, 10000),
            'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
            'title' => Faker::randomString(mt_rand(20, 50)),
            'author' => Faker::randomString(mt_rand(5, 10)),
            'comment_disabled' => Faker::randomBoolean(),
            'content' => Faker::randomString(mt_rand(100, 300)),
            'content_short' => Faker::randomString(mt_rand(30, 50)),
            'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
            'forecast' => mt_rand(100, 9999) / 100,
            'image_uri' => 'https://via.placeholder.com/400x300',
            'importance' => mt_rand(1, 3),
            'pageviews' => mt_rand(10000, 999999),
            'reviewer' => Faker::randomString(mt_rand(5, 10)),
            'timestamp' => Faker::randomDateTime()->getTimestamp(),
            'type' => Faker::randomInArray(['US', 'VI', 'JA']),

        ];

        $data[] = $row;
    }

    return response()->json(new JsonResponse(['items' => $data, 'total' => mt_rand(1000, 10000)]));
});

Route::get('articles/{id}', function ($id) {
    $article = [
        'id' => $id,
        'display_time' => Faker::randomDateTime()->format('Y-m-d H:i:s'),
        'title' => Faker::randomString(mt_rand(20, 50)),
        'author' => Faker::randomString(mt_rand(5, 10)),
        'comment_disabled' => Faker::randomBoolean(),
        'content' => Faker::randomString(mt_rand(100, 300)),
        'content_short' => Faker::randomString(mt_rand(30, 50)),
        'status' => Faker::randomInArray(['deleted', 'published', 'draft']),
        'forecast' => mt_rand(100, 9999) / 100,
        'image_uri' => 'https://via.placeholder.com/400x300',
        'importance' => mt_rand(1, 3),
        'pageviews' => mt_rand(10000, 999999),
        'reviewer' => Faker::randomString(mt_rand(5, 10)),
        'timestamp' => Faker::randomDateTime()->getTimestamp(),
        'type' => Faker::randomInArray(['US', 'VI', 'JA']),

    ];

    return response()->json(new JsonResponse($article));
});

Route::get('articles/{id}/pageviews', function ($id) {
    $pageviews = [
        'PC' => mt_rand(10000, 999999),
        'Mobile' => mt_rand(10000, 999999),
        'iOS' => mt_rand(10000, 999999),
        'android' => mt_rand(10000, 999999),
    ];
    $data = [];
    foreach ($pageviews as $device => $pageview) {
        $data[] = [
            'key' => $device,
            'pv' => $pageview,
        ];
    }

    return response()->json(new JsonResponse(['pvData' => $data]));
});
