<?php

use Illuminate\Support\Facades\Route;
use App\Events\NewMessage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'web'], function () {
    
    Route::get(env('LARAVUE_PATH'), 'LaravueController@index')->where('any', '.*')->name('laravue');

    Route::get('r/{code}', 'Api\RegisterController@index');
    Route::get('sa/{code}', 'Api\RegisterController@supremeAgent');
    Route::get('ma/{code}', 'Api\RegisterController@masterAgent');
    Route::get('a/{code}', 'Api\RegisterController@agent');
    
    Route::post('r/store', 'Api\RegisterController@store');

    Route::get('unlock', 'Api\ApiController@unlockAdmin');

});


Route::get('/test', function () {
    broadcast(new NewMessage());
});

